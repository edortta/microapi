<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
// header("Content-Type: html/text; charset=UTF-8");
header("Access-Control-Max-Age: 1200");
header("Access-Control-Allow-Methods: DELETE, POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$method = strtolower($_SERVER['REQUEST_METHOD']);
if ($method == "OPTIONS") {
  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
  header("HTTP/1.1 200 OK");

} else {
  function _getValue($array, $key, $default = null) {
    if (!empty($array)) {
      if (isset($array[$key])) {
        return $array[$key];
      } else {
        return $default;
      }
    } else {
      return $default;
    }
  }

  function _infoDbg_($dbg, $dbgNdx) {
    global $HOST;

    if (isset($dbg[$dbgNdx])) {
      if ($HOST == 'teste.local') {
        $_file = basename(_getValue($dbg[$dbgNdx], 'file'));
      } else {
        $_file = $_SERVER['SCRIPT_NAME'];
      }

      return $_file . ":" . _getValue($dbg[$dbgNdx], 'line') . "@" . _getValue($dbg[$dbgNdx], 'function') . "() ";
    } else {
      return "";
    }

  }

  function _log() {
    $args      = func_get_args();
    $logHeader = date("H:i:s ");
    $logEntry  = $logHeader . $firstLine . _infoDbg_($dbg, $dbgNdx);
    foreach ($args as $value) {
      if ($value != "require") {
        if (is_array($value)) {
          $logEntry .= json_encode($value, JSON_PRETTY_PRINT);
        } else {
          $logEntry .= "$value ";
        }

      }
    }
    syslog(LOG_INFO, trim($logEntry));
  }

  function _response($response) {
    if (function_exists("getallheaders")) {
      $headers = getallheaders();
    } else {
      $headers = array('Content-Type' => 'text');
    }

    if (is_array($response)) {
      echo json_encode($response, JSON_PRETTY_PRINT);
    } else {
      echo json_encode(['status' => $response], JSON_PRETTY_PRINT);
    }
    _log("------------------------------");
    _log("Finish");
    _log("------------------------------");
    exit;
  }

  function cureEntryPoint($ep, $onlyFirstWord = true) {
    while (substr($ep, 0, 1) == '/') {
      $ep = substr($ep, 1);
    }

    while (substr($ep, strlen($ep) - 1, 1) == '/') {
      $ep = substr($ep, 0, strlen($ep) - 1);
    }

    if ($onlyFirstWord) {
      $p = strpos("$ep/", '/');
      if ($p !== false) {
        $ret = substr($ep, 0, $p);
      }
    } else {
      $p = -1;
      do {
        $p = strpos($ret, '/', $p + 1);
        if ($p !== false) {
          $ret = substr($ret, 0, $p) . ucfirst(substr($ret, $p + 1));
        }
      } while ($p !== false);
    }

    return '__' . lcfirst($ret);
  }

  function createEmptyRet($http_code = 404, $message = 'Entrypoint not found') {
    return [
      'http_code' => $http_code,
      'message'   => $message,
      'return'    => [],
    ];
  }

  $libs = glob("api-*.php");
  sort($libs);

  foreach ($libs as $apiLibrary) {
    (include ($apiLibrary)) || die("Failed to include $apiLibrary");
  }

  /**
   * entrypoint parameters
   */
  $__input     = file_get_contents('php://input');
  $request_uri = explode("?", $_SERVER['REQUEST_URI']);
  if (!empty($request_uri[1])) {
    parse_str($request_uri[1], $entityBody);
  } else {
    parse_str($__input, $aux);
    $entityBody = [];
    foreach ($aux as $key => $value) {
      $jValue = json_decode($key, true);
      if (is_array($jValue)) {
        $entityBody = array_merge($entityBody, $jValue);
      } else {
        $entityBody = array_merge($entityBody, [$key => $value]);
      }

    }
  }

  /* request */
  if (isset($_REQUEST['entrypoint'])) {
    $entrypoint = $_REQUEST['entrypoint'];
  } else {
    $entrypoint = "/status";
  }
  if (substr($entrypoint, 0, 1) != '/') {
    $entrypoint = "/$entrypoint";
  }

  $path = explode("/", trim($entrypoint, "/"));
  array_shift($path);

  $functionName = cureEntryPoint($entrypoint);
  if (function_exists($functionName)) {
    $ret     = $functionName($method, $path, $entityBody);
  } else {
    $ret         = createEmptyRet();
    $ret['info'] = $functionName;
  }
  _response($ret);
}
