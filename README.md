# microAPI

Experimental micro API developed in PHP without dependencies.

## TL;DR

`.htaccess `redirects everything starting with `api/`to`api.php`. This in turn loads `api-implementation.php`.

If you call `/api/info `the `__info()` function (that resides in `api-implementation.php`) will be executed.

This means that if you write a function called `HiFriendlyWorld()` it can be called by `/api/HiFriendlyWorld`.


## Little history

This project was developed for the sole purpose of making the development of small Restful APIs simpler.

However, it is (and always will be) an experimental project. Simple enough for any beginning programmer to understand but promising enough for a more seasoned programmer use as the basis for a larger project.

It is important that your Apache2 is configured to use mod_rewrite. Maybe you want mod_header enabled too.

## The idea

To keep the things simpler as possible.